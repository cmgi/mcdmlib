import numpy as np
import mcdm.ahp as ahp


#Criterios --> Precio, Confort, Motor, Equipamiento
#Alternativas --> Seat Marbella, Peugeot e208, Renault Megane

print("Objetivo --> Comprar un coche acorde a nuestros criterios dentro de una serie de alternativas")
print()

seatMarbella = [9, 3, 3, 2]
peugeotE208 = [2, 7, 7, 7]
renaultMegane = [5, 7, 6, 8]

crit = int(input("Introduzca el número de criterios --> "))
criterios = []

for x in range (0, crit):
    criterio = input("Introduzca el criterio a valorar: ")
    criterios.append(criterio)

matrizPrueba = np.ones((len(criterios),len(criterios)))


for x in range(0, matrizPrueba.shape[0]):
    for y in range(0, matrizPrueba.shape[0]):
        if x == y:
            matrizPrueba[x][y] = 1
        else:
            if x < y:
                print(criterios[x], "sobre", criterios[y])
                escala = int(input(" --> "))
                matrizPrueba[x][y] = escala
                if escala == 0:
                    matrizPrueba[y][x] = 0
                else:
                    matrizPrueba[y][x] = 1/escala

print(matrizPrueba)

metodoAhp = ahp.Ahp(matrizPrueba)
print("Seat Marbella --> ", metodoAhp.ahp(seatMarbella))
print("Peugeot e208 --> ", metodoAhp.ahp(peugeotE208))
print("Renault Megane -->", metodoAhp.ahp(renaultMegane))








