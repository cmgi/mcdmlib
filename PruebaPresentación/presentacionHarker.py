import numpy as np
import mcdm.ahp as ahp
import mcdm.harker as hkr

#Criterios --> Precio, Confort, Motor, Equipamiento
#Alternativas --> Seat Marbella, Peugeot e208, Renault Megane

print("Objetivo --> Comprar un coche acorde a nuestros criterios dentro de una serie de alternativas")
print()

seatMarbella = [9, 3, 3, 2]
peugeotE208 = [2, 7, 7, 7]
renaultMegane = [5, 7, 6, 8]

crit = int(input("Introduzca el número de criterios --> "))
criterios = []

for x in range (0, crit):
    criterio = input("Introduzca el criterio a valorar: ")
    criterios.append(criterio)

matrizPrueba = np.ones((len(criterios),len(criterios)))


for x in range(0, matrizPrueba.shape[0]):
    for y in range(0, matrizPrueba.shape[0]):
        if x == y:
            matrizPrueba[x][y] = 1
        else:
            if x < y:
                print(criterios[x], "sobre", criterios[y])
                escala = int(input(" --> "))
                matrizPrueba[x][y] = escala
                if escala == 0:
                    matrizPrueba[y][x] = 0
                else:
                    matrizPrueba[y][x] = 1/escala

print(matrizPrueba)

print("----------------------------------------------------------------------")
print()
print("Solución aplicando el método de reconstrucción de matrices de Harker")

solucionHarker = hkr.Harkerprediction.prediction((1,3), matrizPrueba)
print("La matriz resultante es: ", solucionHarker)

metodoAhp = ahp.Ahp(solucionHarker)
print("Seat Marbella --> ", metodoAhp.ahp(seatMarbella))
print("Peugeot e208 --> ", metodoAhp.ahp(peugeotE208))
print("Renault Megane -->", metodoAhp.ahp(renaultMegane))