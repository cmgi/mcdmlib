import configparser

import DebugPrint as DebugPrint


class ConfigLoader:
    """Loads the configuration file from a path"""
    def __init__(self, path):
        self._cfg = configparser.ConfigParser()
        self._cfg.read(path)

    def Section(self, section):
        """Returns a dictionary from a section of the configuration file"""
        dict1 = {}
        options = self._cfg.options(section)
        for option in options:
            try:
                dict1[option] = self._cfg.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def ListSections(self):
        """Returns a list of the sections in the configuration file"""
        return self._cfg.sections()