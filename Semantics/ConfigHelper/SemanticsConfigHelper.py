import configparser
from . import ConfigLoader

class SemanticsConfigHelper(ConfigLoader.ConfigLoader):
    """Configuration helper for the semantics module"""

    def DefaultLocale(self):
        """Returns the default locale indicated in the config file"""
        return self.Section("LanguageDetector")['defaultlocale']

    def SentimentDetectors(self):
        """Returns the installed sentiment detectors as indicated in the config file"""
        sentiment_detectors = []
        for key in self.Section("SentimentDetector"):
            sentiment_detectors.append(key)
        return sentiment_detectors
    