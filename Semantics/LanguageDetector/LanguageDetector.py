import langid
from langdetect import detect
import textblob

class LanguageDetector:
    """Provides methods for assessing the language of a given text"""

    def __init__(self, locale = "NaN"):
        """Define the default Locale, e.g. 'es' for Spanish"""
        if(locale == ""):
            self._predeflocal = "NaN"
        else:
            self._predeflocal = locale
    def Langid_Safe(self, txt):
        """CAUTION: VERY PRONE TO ERRORS. Determines the language of a given test using the langid module"""
        try:
            return langid.classify(txt)[0]
        except Exception as e:
            pass
        
    def Langdetect_Safe(self, txt):
        """CAUTION: VERY PRONE TO ERRORS. Determines the language of a given test using the langdetect module"""
        try:
            return detect(txt)
        except Exception as e:
            pass

    def Textblob_Safe(self, txt):
        """ CAUTION: VERY PRONE TO ERRORS. Determines the language of a given test using the textblob module"""
        try:
            return textblob.TextBlob(txt).detect_language()
        except Exception as e:
            pass

    def LocaleOf(self, txt):
        """Determines the language of a ginven text by consensus."""
        try:
            locales = []
            #Use the preconf. language assessment methods to try to identify the locale
            locales.append(self.Langdetect_Safe(txt))
            locales.append(self.Langid_Safe(txt))
            locales.append(self.Textblob_Safe(txt))
            #Check if all locales are equivalent
            lastitem = locales[0]
            for l in locales:
                if l != lastitem:
                    return self._predeflocal #If one of the assessment methods differs returns the default locale
                lastitem = l
            return lastitem
        except Exception as e:
            pass