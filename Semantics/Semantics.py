from .LanguageDetector import LanguageDetector
from .ConfigHelper import SemanticsConfigHelper
#Imports the spanish sentiment detector directly for testing purposes
from .SentimentDetector import Training


class Semantics:
    def __init__(self, defaultlocale):
        self.defaultlocale = defaultlocale
        self._langdetect = LanguageDetector.LanguageDetector(self.defaultlocale)

    def DetectLanguage(self, text):
        return self._langdetect.LocaleOf(text)

    def TrainSentimentDetector(self, texts, polarity):
        sp = Training.ClassifierFactory(self.defaultlocale, texts, polarity)
        return sp.Train()
