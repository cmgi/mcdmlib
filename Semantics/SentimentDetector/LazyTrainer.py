import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.data import load
from nltk.stem import SnowballStemmer
from string import punctuation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC, SVC
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn import metrics
import numpy as np
import pickle

non_words = list(punctuation)
stemmer = SnowballStemmer('spanish')
lang_stopwords = stopwords.words('spanish')
non_words.extend(['¡', '¿'])
non_words.extend(map(str,range(10)))

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed
        

def tokenize(text):
    text = ''.join([c for c in text if c not in non_words])
    tokens =  word_tokenize(text)
    try:
        stems = stem_tokens(tokens, stemmer)
    except Exception as e:
        print(e)
        print(text)
        stems = ['']
    return stems
    
        

def Train(texts, targets):
    print("Training semantic classifier")

    vectorizer = CountVectorizer(
                        analyzer = 'word',
                        tokenizer = tokenize,
                        lowercase = True,
                        stop_words = lang_stopwords)
    pipeline = Pipeline([
        ('vect', CountVectorizer(
                analyzer = 'word',
                tokenizer = tokenize,
                lowercase = True,
                stop_words = lang_stopwords,
                min_df = 50,
                max_df = 1.9,
                ngram_range=(1, 1),
                max_features=1000
                )),
        ('cls', MLPClassifier(max_iter = 1000, tol = 0.0001)),
        #('cls', LinearSVC(C=.2,
        #                  loss='squared_hinge',
        #                  max_iter=5000,
        #                  multi_class='crammer_singer',
        #                  random_state=None,
        #                  penalty='l2',
        #                  tol=0.0001,
        #         )),
       ])
    
    parameters = {
            'vect__max_df': (0.5, 1.9),
            'vect__min_df': (10, 20,50),
            'vect__max_features': (500, 1000),
            'vect__ngram_range': ((1, 1), (1, 2)), 
            'cls__C': (0.2, 0.5, 0.7),
            'cls__loss': ('hinge', 'squared_hinge'),
            'cls__max_iter': (500, 1000)
        }
    pipeline.fit(texts, targets)
    return pipeline

