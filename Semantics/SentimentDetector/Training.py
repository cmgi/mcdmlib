from . import LazyTrainer
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.data import load
from nltk.stem import SnowballStemmer
from string import punctuation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn import metrics
import numpy as np


class SentimentClassifier():
    """ Semantic Classifier for sentiment analysis: polarity and intensity """
    def __init__(self, locale, polarity_classifier, performance_metrics, detailedreport = 'Not available'):
        self.recall_polartiy = performance_metrics['recall_polartiy']
        self.accuracy_polartiy = performance_metrics['accuracy_polartiy']
        self.presicion_polartiy = performance_metrics['presicion_polartiy']
        self.f1_polartiy = performance_metrics['f1_polartiy']
        self.locale = locale
        self.polarity_classifier = polarity_classifier
        self.detailedreport = detailedreport
    
    def Classify(self, text):
        """ Returns the sentiment of a given text"""
        return self.polarity_classifier.predict(text)
        

class ClassifierFactory():
    """Generates trained semantic classifiers from a given corpus.
       Supported locales:
       'es' (Spanish)  
       'en' (English)  
       'pt' (Portuguese)
       'fr' (French)
       'de' (German)
       'it' (Italian)
    """
    def __init__(self, locale, corpus_phrases, corpus_polarity):
        #Select the correct stemmer and stopwords list for the given language
        switcher = {
            'es':{ 'stemmer': SnowballStemmer('spanish'), 'stopwords': stopwords.words('spanish')},
            'en':{ 'stemmer': SnowballStemmer('english'), 'stopwords': stopwords.words('english')},
            'pt':{ 'stemmer': SnowballStemmer('portuguese'), 'stopwords': stopwords.words('portuguese')},
            'fr':{ 'stemmer': SnowballStemmer('french'), 'stopwords': stopwords.words('french')},
            'de':{ 'stemmer': SnowballStemmer('german'), 'stopwords': stopwords.words('german')},
            'it':{ 'stemmer': SnowballStemmer('italian'), 'stopwords': stopwords.words('italian')},
            }
        config = switcher.get(locale)
        if(config == None):
            raise AttributeError('Specified locale is unsupported. Check class information for reference.')
        self.locale = locale
        #Assigns local variables
        self.corpus_phrases = corpus_phrases
        self.corpus_polarity = corpus_polarity
        self.stemmer = config['stemmer']
        self.stopwords = config['stopwords']

    def Train(self):
        """Trains the semantic classifier"""
        #Calls LazyTrainer script for simple SVM pipeline training
        LazyTrainer.stemmer = self.stemmer
        LazyTrainer.lang_stopwords = self.stopwords
        #Trains polarity and intesity classifer
        polarity_classifier =  LazyTrainer.Train(self.corpus_phrases, self.corpus_polarity)
        #Performance metris calculation
        predicted_polarity = polarity_classifier.predict(self.corpus_phrases)
        performance_metrics = {
                                'recall_polartiy': recall_score(self.corpus_polarity, predicted_polarity, average='micro'),
                                'accuracy_polartiy': accuracy_score(self.corpus_polarity, predicted_polarity),
                                'presicion_polartiy': precision_score(self.corpus_polarity, predicted_polarity, average='micro'),
                                'f1_polartiy': f1_score(self.corpus_polarity, predicted_polarity, average='micro'),
                              }
        #polarity_target_names = ['Very Negative', 'Negative', 'Neutral', 'Positive', 'Very Positive']
        polarity_target_names = ['Negative', 'Positive']
        #polarity_target_names = ['Negative', 'Neutral', 'Positive']
        
        
        
        report_polarity = classification_report(self.corpus_polarity, predicted_polarity, target_names = polarity_target_names)
        #Returns a new semantic classifier
        #print('TEST:')
        #print(polarity_classifier.predict(['Auronplay es un mierda']))
        return SentimentClassifier(self.locale, polarity_classifier, performance_metrics, report_polarity)
