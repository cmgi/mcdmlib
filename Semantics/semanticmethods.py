import pickle
import os
import warnings
from Semantics.SentimentDetector.Training import SentimentClassifier


def classify_text(text):
    """
    Simple classifier that returns the polarity of a sentence.
    :param text: A string contining a sentence.
    :return: -1 if the sentence expresses negative emotion and 1 if it expresses positive emotion.
    """
    warnings.simplefilter("ignore")
    classifier = pickle.load(open(os.path.join("Semantics", "semantic_classifier.p"), 'rb'))
    return classifier.Classify([text])[0]


def classify_text_list(text_list):
    """
    Simple classifier that can assess the polarity of a list of sentences
    :param text_list: A list of strings containing sentences.
    :return: A list of -1 and 1 containing the emotion of each sentence.
    """
    warnings.simplefilter("ignore")
    classifier = pickle.load(open(os.path.join("Semantics", "semantic_classifier.p"), 'rb'))
    return list(classifier.Classify(text_list))
