import requests
import base64


class TwitterExtractor:
    """
    This class implements the needed methods to get communications from Twitter.
    For the usage of this method, you will need the client_key and the client_secret from a developer
        twitter account. More information in https://developer.twitter.com/
    :param client_key: API Client Key provided by twitter.
    :param client_secret: API Secret provided by twitter.
    """
    def __init__(self, client_key, client_secret):
        key_secret = '{}:{}'.format(client_key, client_secret).encode('ascii')

        # Client & secret
        self.client_key = client_key
        self.client_secret = client_secret
        self.b64_encoded_key = base64.b64encode(key_secret)
        self.b64_encoded_key = self.b64_encoded_key.decode('ascii')

    def get_communications(self, query, n=100):
        """
        Returns the most recent n Tweets that matches the query.
        :param query: A string containing the text to search in Twitter API.
        :param n: Number of desired Tweets. Max=100.
        :return: List of the text of the n most recent Tweets matching the query.
        """
        base_url = 'https://api.twitter.com/'
        auth_url = '{}oauth2/token'.format(base_url)

        # Get Access token.
        auth_headers = {
            'Authorization': 'Basic {}'.format(self.b64_encoded_key),
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        }

        auth_data = {
            'grant_type': 'client_credentials'
        }
        auth_resp = requests.post(auth_url, headers=auth_headers, data=auth_data)
        access_token = auth_resp.json()['access_token']

        # Twitter Search
        search_headers = {
            'Authorization': 'Bearer {}'.format(access_token)
        }

        search_params = {
            'q': query,
            'lang': 'es',
            'result_type': 'recent',
            'count': n
        }

        search_url = '{}1.1/search/tweets.json'.format(base_url)

        search_resp = requests.get(search_url, headers=search_headers, params=search_params)
        if search_resp.status_code in [401, 403]:
            raise ConnectionRefusedError("Twitter API rejected the request because client_key "
                                         "or client_secret are not valid")
        if search_resp.status_code != 200:
            raise ConnectionError("Could not retrieve the requested information from Twitter API.")
        tweet_data = search_resp.json()

        # Extract the tweet text from the response
        res = list()
        for t in tweet_data['statuses']:
            res.append(t['text'])

        return res
