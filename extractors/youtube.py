import requests


class YouTubeExtractor:
    """
    This class implements the needed methods to get communications from YouTube.
    For the usage of this method, you will need the client_key and the client_secret from a developer
        google account and acces to the YouTube API. More information in https://developers.google.com/
    :param key: key provided by Google with access to YouTube API.
    """
    def __init__(self, key):
        self.key = key
        self.base_url = "https://www.googleapis.com/youtube/v3/"

    def get_communications(self, query, n=20, v=5):
        """
        Gets n communications from v videos matching the query.
        :param query: A string containing the text to search in YouTube API
        :param n: Nunmber of desired comments for each video.
        :param v: Number of video from which to extract comments.
        :return: A list containing a maximum of n*v comments from YouTube matching the query.
        """

        # Get v video IDs matching the query.
        params = {
            "part": "snippet",
            "q": "universidad",
            "order": "relevance",
            "maxResults": 5,
            "type": "video",
            "key": self.key
        }
        req_res = requests.get(url=self.base_url + "search", params=params)
        if req_res.status_code in [401, 403]:
            raise ConnectionRefusedError("YouTube API rejected the request because your key is not valid")
        if req_res.status_code != 200:
            raise ConnectionError("Could not retrieve the requested information from YouTube API.")

        items = req_res.json()['items']
        ids = [x['id']['videoId'] for x in items]

        # Get CommentThreads for each video.
        comment_threads = list()
        for vid in ids:
            params = {
                "part": "snippet",
                "videoId": vid,
                "order": "relevance",
                "maxResults": 20,
                "key": self.key
            }
            req_res = requests.get(url=self.base_url + "commentThreads", params=params)
            if req_res.status_code in [401, 403]:
                raise ConnectionRefusedError("YouTube API rejected the request because your key is not valid")
            if req_res.status_code != 200:
                raise ConnectionError("Could not retrieve the requested information from YouTube API.")
            comment_threads.append(req_res.json())

        # Extract the text of the top level comment for each CommentThread
        res = list()
        for c in comment_threads:
            for itm in c['items']:
                try:
                    res.append(itm['snippet']['topLevelComment']['snippet']['textDisplay'])
                except KeyError:
                    pass
        return res
