# AHP Method (Analytic Hierarchy Process)
# Consistency Indexes (Saaty CI, Saaty CR, CI*, CI+, CR+)

import numpy as np


class Ahp:
    """
    This class implements all necessary methods to calculate AHP.
    ----------
    Parameters
    matrix: Peer comparison matrix.
    """

    def __init__(self, matrix):
        if not isinstance(matrix, np.ndarray):
            matrix = np.asmatrix(matrix)
        self.matrix = matrix
        self.normalizedautovector = self._normalize(matrix)

    @staticmethod
    def _checkdiagonal(matrix):
        """
        Check that the diagonal is correct
        :param matrix:
        :return:
        """
        assert isinstance(matrix, np.ndarray)

        for i in range(matrix.shape[0]):
            if matrix[i][i] != 1:
                return False
        return True

    @staticmethod
    def _inversecalculations(matrix):
        """
        Calculates the inverses in those positions that are zero
        :param matrix:
        :return: matrix
        """
        assert isinstance(matrix, np.ndarray)

        matrixsize = matrix.shape[0]
        for i in range(matrixsize):
            for j in range(matrixsize):
                if matrix[i][j] == 0:
                    matrix[i][j] = matrix[0][0] / matrix[j][i]
        return matrix

    @staticmethod
    def _autovalues(matrix):
        """
        Calculates autovalues of matrix and returns the maximum autovalue and its index
        :param matrix:
        :return: Maximum autovalue and index
        """
        assert isinstance(matrix, np.ndarray)
        autovalues = np.linalg.eigvals(matrix)
        maximumautovalue = (max(autovalues.real))
        maximumautovalueindex = np.argmax(autovalues)

        autovalueresult = [maximumautovalue, maximumautovalueindex]

        return autovalueresult

    @staticmethod
    def _autovectors(matrix):
        """
        Find autovectors of the matrix and calculate the associated autovector to the maximum autovalue
        of this matrix
        :param matrix:
        :return: Maximum autovector
        """
        assert isinstance(matrix, np.ndarray)

        maximumautovalue = Ahp._autovalues(matrix)
        autovectors = np.linalg.eig(matrix)
        autovectorsmatrix = autovectors[1]

        for x in range(matrix.shape[0]):
            maximumautovector = autovectorsmatrix[:, maximumautovalue[1]]

        return [x.real for x in maximumautovector]

    @staticmethod
    def _normalize(matrix):
        """
        This method normalizes the vector that returns the previous method
        :param matrix:
        :return: Normalized vector
        """

        assert isinstance(matrix, np.ndarray)

        maximumautovector = Ahp._autovectors(matrix)
        normalized = [(x / sum(maximumautovector)) for x in maximumautovector]

        return normalized

    def _calculatingalternativevalue(self, matrix, alternativevalue):
        """
        This method calculates the value of the alternative using the normalized vector and the values of
        each alternative.
        :param matrix:
        :param alternativevalue:
        :return: Sum of each alternative
        """

        normalizedautovector = self.normalizedautovector

        for x in range(0, len(alternativevalue)):

            valueofalternative = [x * y for x, y in zip(alternativevalue, normalizedautovector)]
            alternativesum = 0

            for v in range(len(alternativevalue)):
                alternativesum = alternativesum + valueofalternative[v]

        return float(alternativesum)

    @staticmethod
    def _totalalternatives(matrix, alternativesvalues):
        """
        Does the same as the previous method for a list of alternatives
        :param matrix:
        :param alternativesvalues:
        :return: Sum of each alternative in a list
        """
        return [Ahp._calculatingalternativevalue(matrix, x) for x in alternativesvalues]

    def get_criteria_weights(self):

        return self.normalizedautovector

    def ahp(self, alternative):
        """
        This method evaluates an alternative using the AHP method
        :param alternative: Vector of valuations for each of the criteria of an alternative
        :return: Valuation of the alternative according to the AHP method
        """

        values = self._calculatingalternativevalue(self.matrix, alternative)

        return values


class AhpConsistency:
    """
    This class contains the methods for calculating consistency indexes AHP
    ---------------
    Parameters
    ahp: Instance of AHP class which consistency calculations methods are applied
    """

    def __init__(self, ahp):
        assert isinstance(ahp, Ahp)
        self.ahp = ahp
        self.matrix = ahp.matrix
        self.autovalue = Ahp._autovalues(self.matrix)[0]

    @staticmethod
    def _factorial(num):
       if num==0 or num==1:
           res = 1
       elif num>1:
           res = num*AhpConsistency._factorial(num-1)
       return res


    def get_autovalue(self):

        return self.autovalue

    def ci(self):
        """
        Calculates Saaty consistency index
        :return: Saaty consistency index
        """
        autovalue = Ahp._autovalues(self.matrix)[0]
        matrixorder = self.matrix.shape[0]
        saatyindex = (autovalue - matrixorder) / (matrixorder - 1)

        return saatyindex

    def cr(self):
        """
        Calculates Saaty consistency ratio
        :return: Saaty consistency ratio
        """
        ratiovalues = [0, 0, 0, 0.52, 0.88, 1.11, 1.25, 1.34, 1.40, 1.48, 1.51, 1.53, 1.57, 1.58, 1.59]
        consistencyindex = self.ci()
        if abs(consistencyindex) < 1e-10:
            return 0

        try:
            ratioconsistency = ratiovalues[self.ahp.matrix.shape[0]]
        except:
            ratioconsistency = 1.6

        return consistencyindex / ratioconsistency

    @staticmethod
    def recursive_decomposer(matrix):
        """
        Pick nxn matrix and convert it to 3x3 matrix
        :param matrix:
        :return: All 3x3 matrix in nxn matrix.
        """
        matrices = []

        if matrix.shape[0] > 3:

            for x in range(matrix.shape[0]):

                nmat = np.delete(matrix, x, axis = 0)
                nmat = np.delete(nmat, x, axis = 1)

                if nmat.shape[0] > 3:

                    for i in AhpConsistency.recursive_decomposer(nmat):

                        matrices.append(i)

                else:

                    matrices.append(nmat)

        return matrices

    def ci_star(self):

        """
        This method calculates ci_star index without interval
        :return:ci_star consistency index
        """

        submatrixlist = AhpConsistency.recursive_decomposer(self.matrix)

        if len(submatrixlist) > 0:

            submatrixdeterminant = np.linalg.det(submatrixlist)
            cistarindexsolution = np.mean(submatrixdeterminant)
            return cistarindexsolution

        elif len(submatrixlist) == 0:

            submatrixdeterminant = np.linalg.det(self.matrix)
            return submatrixdeterminant

    def ci_plus(self, scale='saaty'):

        """
        This method calculates ci_plus index depending on the scale indicated by parameter
        :param scale: One of the scales accepted for this index
        :return: ci_plus consistency index and whether it is accepted or not
        """

        boundaryindexsum = 0
        dimension = self.matrix.shape[0]

        if scale == 'saaty':
            phi = 531442/729
        elif scale == 'additive':
            phi = 100.001
        elif scale == 'fuzzy':
            phi = 100.001

        submatrixlist = AhpConsistency.recursive_decomposer(self.matrix)
        totalmatrix = len(submatrixlist)

        if dimension < 3:
            return 0

        elif dimension > 3:
            for x in submatrixlist:
                boundaryindex = (phi - (abs(x[0][2]/(x[0][1]*x[1][2])) + abs((x[0][1]*x[1][2])/x[0][2])))/(phi-2)
                boundaryindexsum = boundaryindexsum + boundaryindex

            boundarytotalindex = (1/totalmatrix)*boundaryindexsum

        elif dimension == 3:
            boundarytotalindex = (phi - (abs(self.matrix[0][2]/(self.matrix[0][1]*self.matrix[1][2]))
                                    + abs((self.matrix[0][1]*self.matrix[1][2])/self.matrix[0][2])))/(phi-2)

        if scale == 'saaty':

           percentilvalues = [0, 0, 0, 0.998, 0.993, 0.981, 0.971, 0.965, 0.961, 0.9590]
           matrixpercentilvalue = percentilvalues[dimension]

           if boundarytotalindex >= matrixpercentilvalue:

               return boundarytotalindex, True

           else:

               return boundarytotalindex, False

    def cr_plus(self):
        """
        Calculates CR+ critical value
        :return: True if matrix is accepted or False if matrix is not accepted
        """
        criticalvalues = [0, 0, 0, 1.3834, 1.3815, 1.3677, 1.2713, 1.2130, 1.2088, 1.1657]
        consistencyindex = self.ci_plus()
        if consistencyindex[1] == True:
            criticalvalue = (criticalvalues[self.ahp.matrix.shape[0]], True)
        elif consistencyindex[1] == False:
            criticalvalue = (criticalvalues[self.ahp.matrix.shape[0]], False)

        return criticalvalue


