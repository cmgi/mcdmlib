#Comparison Matrix Geometric Index (CMGI)

import numpy as np

class cmgi:
    """
    This class implements all necessary methods to calculate CMGI.
    """
    def __init__(self):
        pass

    @staticmethod
    def calculateOPM(polaritytuples):
        """
        Calculate OPM matrices from polarized opinions.
        :param polaritytuples:
        :return: OPM matrices list
        """

        dimension = len(polaritytuples)
        opmmatrix = np.ones((dimension, dimension))
        matrixlist = []

        for z in range(dimension):
            actualpolarity = polaritytuples[z]
            for x in range(len(actualpolarity)):
                for y in range(len(actualpolarity)):
                    if actualpolarity[x] == actualpolarity[y]:
                        opmmatrix[x][y] = 1
                    elif actualpolarity[x] == 1 and opmmatrix[x][y] == 1:
                        opmmatrix[x][y] = 3
                        opmmatrix[y][x] = 1/3
                    elif actualpolarity[x] == 1 and opmmatrix[x][y] == 3:
                        opmmatrix[x][y] = 9
                        opmmatrix[y][x] = 1/9
                    elif actualpolarity[x] == -1 and opmmatrix[x][y] == 1:
                        opmmatrix[x][y] = 1/3
                        opmmatrix[y][x] = 3
                    elif actualpolarity[x] == -1 and opmmatrix[x][y] == 3:
                        opmmatrix[x][y] = 1
                        opmmatrix[y][x] = 1
                    elif actualpolarity[x] == -1 and opmmatrix[x][y] == 1/3:
                        opmmatrix[x][y] = 1/9
                        opmmatrix[y][x] = 9
            matrixlist.append(opmmatrix)
            opmmatrix = np.ones((dimension, dimension))

        return matrixlist


    @staticmethod
    def calculateAPM(polaritytuples):
        """
        Transform OPM matrices into an APM matrix using geometric mean.
        :param polaritytuples:
        :return: APM Matrix
        """

        matrixlist = cmgi.calculateOPM(polaritytuples)
        dimension = len(polaritytuples)
        apmmatrix = np.ones((dimension, dimension))

        for z in range(dimension):
            actualmatrix = matrixlist[z]
            for x in range (len(actualmatrix)):
                for y in range (len(actualmatrix)):
                    apmmatrix[x][y] = apmmatrix[x][y] * actualmatrix[x][y]

        for x in range(len(actualmatrix)):
            for y in range(len(actualmatrix)):

                apmmatrix[x][y] = apmmatrix[x][y]**(1/dimension)

        return apmmatrix

    @staticmethod
    def calculatenormalized(polaritytuples):
        """
        Obtain Comparison Matrix Geometric Vector (CMGV) from APM and obtain Comparison Matrix Geometric Index
        (CMGI) from CMGV, then normalize it.
        :param polaritytuples:
        :return: Normalized CMGI vector.
        """

        matrix = cmgi.calculateAPM(polaritytuples)
        autovalues = np.linalg.eigvals(matrix)
        maximumautovalue = (max(autovalues.real))
        maximumautovalueindex = np.argmax(autovalues)

        maximumautovalue = [maximumautovalue, maximumautovalueindex]
        autovectors = np.linalg.eig(matrix)
        autovectorsmatrix = autovectors[1]

        for x in range(matrix.shape[0]):
            maximumautovector = autovectorsmatrix[:, maximumautovalue[1]]

        maxautovect = [x.real for x in maximumautovector]
        normalized = [(x / sum(maxautovect)) for x in maxautovect]

        return normalized

    @staticmethod
    def calculatecmgi(polaritytuples):
        """
        Obtain list of tuples with CMGI index and it ranking.
        :param polaritytuples:
        :return: CMGI solution.
        """

        normalizedvector = cmgi.calculatenormalized(polaritytuples)
        cmgilist = []

        for x in range(len(normalizedvector)):

            cmgilist.append((normalizedvector[x], x+1))

        cmgilist = sorted(cmgilist, reverse=True)

        return cmgilist
