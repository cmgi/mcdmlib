#Implementación del método EMPV(Eigenvector Method for Pairwise Voting)

import numpy as np
import itertools

class Empv:
    """
    This class implements all necessary methods to calculate EMPV.
    ----------
    Parameters
    alternativelist: List of problem alternatives
    votes: List of problem votes
    """

    def __init__(self, alternativeslist, votes):

        self.alternativeslist = alternativeslist
        self.votes = votes

    def _countvotes(self):
        """
        This method count votes and put them in a permutations and preferences tuple.
        :return: tuple with votes and preferences
        """
        permutations = list(itertools.permutations(self.alternativeslist, 2))
        preferenceslist = []
        newvoteslist = []

        for x in range (0, len(permutations)):
            preferenceslist.append(0)

        for x in range(0, len(self.votes)):
            votespermutations = list(itertools.combinations(self.votes[x], 2))
            for y in range(0, len(votespermutations)):
                for z in range(0, len(permutations)):
                    if votespermutations[y] == permutations[z]:
                        preferenceslist[z] += 1

        for x in range (0, len(preferenceslist)):
            newvoteslist.append([permutations[x], preferenceslist[x]])
        return newvoteslist

    def _definematrix(self):
        """
        This method put the number of each preferences in a matrix
        :return: matrix empv votes
        """
        dimension = len(self.alternativeslist)
        empvmatrix = np.zeros((dimension, dimension))
        np.fill_diagonal(empvmatrix,1)
        preferenceslist = self._countvotes()
        preferredlist = []
        preferredlistindex = 0

        for x in preferenceslist:
            preferredlist.append(x[1])
        for x in range(0, dimension):
            for y in range(0, dimension):
                if empvmatrix[x][y] != 1:
                    empvmatrix[x][y] = preferredlist[preferredlistindex]
                    preferredlistindex = preferredlistindex + 1
        for x in range (0, dimension):
            for y in range(0, dimension):
                if empvmatrix[x][y] == 0:
                    empvmatrix[x][y] = 1
        return empvmatrix

    def _getempvmatrix(self):
        """
        This method calculates each position in matrix
        :return: matrix with empv numbers
        """
        empvmatrix = self._definematrix()
        dimension = empvmatrix.shape[0]

        for x in range(0, dimension):
            for y in range(0, dimension):
                if x != y and y >= x:
                    empvmatrix[x][y] = empvmatrix[x][y]/empvmatrix[y][x]
                elif x != y and y < x:
                    empvmatrix[x][y] = 1/empvmatrix[y][x]
        return empvmatrix

    def _calculateasociatedeigenvector(self):
        """
        This method calculates eigenvector asociated to maximum eigenvalue and calculates solution of matrix
        :return: list with empv solution (tuple (alternative, value))
        """
        empvmatrix = self._getempvmatrix()

        eigenvectors = np.linalg.eig(empvmatrix)
        eigenvectors = eigenvectors[1]

        for x in range(empvmatrix.shape[0]):
            maximumeigenvector = np.real(eigenvectors[:, 0])

        maximumeigenvector = [(x / sum(maximumeigenvector)) for x in maximumeigenvector]

        empvsolution = zip(maximumeigenvector, self.alternativeslist)
        empvsolution = sorted(empvsolution, reverse = True)

        return empvsolution

    def empv(self):
        """
        This method calculates EMPV
        :return: EMPV solution
        """
        calculateempv = self._calculateasociatedeigenvector()

        return calculateempv
