#Implement Harker prediction method

import mcdm.operators as owa


class Harkerprediction:

    """
    This class implements all necesary methods to use Harker Prediction Method to complete
    consistency matrix.
    """

    @staticmethod
    def prediction(matrixposition, matrix):
        """
        Predict matrix position by harker method, to keep consistency.
        :param matrixposition:
        :param matrix:
        :return: Matrix
        """
        listasol = []

        i = matrixposition[0]
        j = matrixposition[1]

        for x in range(0, matrix.shape[0]):
            if (x != i) and (x != j) and (matrix[i,x] != 0 and matrix[x, j] != 0):
                listasol.append(matrix[i,x] * matrix[x,j])
        res = owa.Owa.calculate_geometric_mean(listasol)

        matrix[i][j] = res
        matrix[j][i] = 1/res

        return matrix


