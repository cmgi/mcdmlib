#Implement interval decision-making model

import numpy as np
import mcdm.operators as owaop

class Intervalar:
    """
    This class implements all necesary methods to rank alternatives by interval decision-making model.
    ----------
    Parameters
    matrix: Interval matrix.
    """

    def __init__(self, intervalmatrix):
        self.intervalmatrix = intervalmatrix

    @staticmethod
    def recursive_decomposer(matrix):
        """
        Decompose matrix with size 4 or more to matrices with size 3.
        :param matrix:
        :return: list of 3x3 matrices
        """
        matrices = []
        if matrix.shape[0] > 3:
            for x in range(matrix.shape[0]):
                nmat = np.delete(matrix, x, axis=0)
                nmat = np.delete(nmat, x, axis=1)
                if nmat.shape[0] > 3:
                    for i in Intervalar.recursive_decomposer(nmat):
                        matrices.append(i)
                else:
                    matrices.append(nmat)
        return matrices

    @staticmethod
    def matrix_upper(intervalmatrix):
        """
        Decompose interval matrix to upper matrix without interval.
        :param intervalmatrix:
        :return: Upper Matrix
        """

        dimension = intervalmatrix.shape[0]
        uppermatrix = np.ones((dimension, dimension), dtype=float)

        for x in range (0,dimension):
            for y in range(0, dimension):

                if x == y:
                    uppermatrix[x][y] = 0.5
                elif x < y:
                    uppermatrix[x][y] = (intervalmatrix[x][y])[1]
                elif x > y:
                    uppermatrix[x][y] = 1 - (intervalmatrix[x][y])[1]

        return uppermatrix

    @staticmethod
    def matrix_lower(intervalmatrix):
        """
        Decompose interval matrix to lower matrix without interval.
        :param intervalmatrix:
        :return: Lower Matrix.
        """
        dimension = intervalmatrix.shape[0]
        lowermatrix = np.ones((dimension, dimension), dtype=float)

        for x in range (0, dimension):
            for y in range(0, dimension):

                if x == y:
                    lowermatrix[x][y] = 0.5
                elif x < y:
                    lowermatrix[x][y] = (intervalmatrix[x][y])[0]
                elif x > y:
                    lowermatrix[x][y] = 1 - (intervalmatrix[x][y])[0]

        return lowermatrix

    def ici_plus(self):
        """
        Create Interval Consistency Index of an interval reciprocal matrix, with it upper and lower matrices.
        :return: Interval Consistency Index
        """

        phi = 100.001
        lowindexsum = 0
        upindexsum = 0
        up = Intervalar.matrix_upper(self)
        low = Intervalar.matrix_lower(self)

        dimension = self.shape[0]

        upsubmatrixlist = Intervalar.recursive_decomposer(up)
        uptotalmatrix = len(upsubmatrixlist)

        lowsubmatrixlist = Intervalar.recursive_decomposer(low)
        lowtotalmatrix = len(lowsubmatrixlist)


        if dimension < 3:
            sol = 0

        elif dimension == 3:
            indiceup = (phi - (abs(up[0][2] / (up[0][1] * up[1][2]))
                               + abs((up[0][1] * up[1][2]) / up[0][2]))) / (phi - 2)
            indicelow = (phi - (abs(low[0][2] / (low[0][1] * low[1][2]))
                               + abs((low[0][1] * low[1][2]) / low[0][2]))) / (phi - 2)

            sol = np.minimum(indicelow, indiceup)

        elif dimension > 3:

            for x in upsubmatrixlist:
                indiceup = (phi - (abs(x[0][2]/(x[0][1]*x[1][2])) + abs((x[0][1]*x[1][2])/x[0][2])))/(phi-2)
                upindexsum = upindexsum + indiceup

            totalup = (1/uptotalmatrix)*upindexsum

            for x in lowsubmatrixlist:
                indicelow = (phi - (abs(x[0][2]/(x[0][1]*x[1][2])) + abs((x[0][1]*x[1][2])/x[0][2])))/(phi-2)
                lowindexsum = lowindexsum + indicelow

            totallow = (1/lowtotalmatrix)*lowindexsum

            sol = np.minimum(totalup, totallow)

        return sol

    @staticmethod
    def interval_reability_index(interval):
        """
        Calculate Interval Reability Index (IRI)
        :param interval:
        :return: Interval Reability Index
        """

        intervalsup = interval[1]
        intervalinf = interval[0]

        if intervalinf == 0 and intervalsup == 0:
            raise ValueError('Both interval cant be 0')

        if intervalinf == 0 and intervalsup != 0:
            iri = 1
        elif intervalinf != 0 and intervalsup == 0:
            iri = -1
        elif intervalsup == intervalinf:
            iri = 0
        else:
            irisup = np.absolute(intervalsup) - np.absolute(intervalinf)
            iriinf = np.absolute(intervalsup) + np.absolute(intervalinf)
            iri = irisup / iriinf

        return iri

    @staticmethod
    def infla_defla_matrix(ismamatrix):
        """
        Deflate an interval matrix
        :param ismamatrix:
        :return: Deflated interval matrix
        """
        res = np.zeros(ismamatrix.shape, dtype=tuple)

        delta = 0.00001

        dimension = ismamatrix.shape[0]

        for x in range(dimension):
            for y in range(dimension):
                if x != y:
                    res[x][y] = (ismamatrix[x][y][0] + delta, ismamatrix[x][y][1] - delta)
                else:
                    res[x][y] = (0.500, 0.500)

        return res

    @staticmethod
    def matrices_IPCM(preferencesmatrix):
        """
        Process for consistency analysis of the IPCM (Interval Pairwise Comparison Matrix)
        :param preferencesmatrix:
        :return: Matrix IPCM and Percentil Value
        """

        dimension = preferencesmatrix.shape[0]
        maxpercentil = 6 #Maximum percentil value (p80) for matrix (Minimum is 0.50)
        percentilvaluelist = [0.9925, 0.9717, 0.9547, 0.9499, 0.9943, 0.9782, 0.9600, 0.9537, 0.9956, 0.9834, 0.9650,
                          0.9575, 0.9965, 0.9872, 0.9698, 0.9614, 0.9969, 0.9900, 0.9746, 0.9652, 0.9978, 0.9921,
                          0.9792, 0.9691, 0.9986, 0.9937, 0.9836, 0.9731]

        percentilvaluematrix = np.array(percentilvaluelist).reshape(7, 4)

        percentilvalue = percentilvaluematrix[maxpercentil][dimension-3]
        M = np.ones((dimension, dimension), dtype=list)
        operatorcount = 0
        percentilecount = 0
        maxop = 10000

        #Step 1
        for x in range(dimension):
            for y in range(dimension):
                if x != y:
                    M[x][y] = owaop.Owa.calculate_isma_owa(preferencesmatrix[x][y], 1)
                else:
                    M[x][y] = [0.500, 0.500]
        #Step 2
        ici = Intervalar.ici_plus(M)
        #Step 3
        if ici < percentilvalue:
            while ici < percentilvalue:
                M = Intervalar.infla_defla_matrix(M)
                ici = Intervalar.ici_plus(M)
                operatorcount = operatorcount + 1
                if operatorcount == maxop:
                    operatorcount = 0
                    percentilecount = percentilecount+1
                    percentilvalue = percentilvaluematrix[maxpercentil-percentilecount][dimension-3]

        return (M, percentilvalue)

    @staticmethod
    def interval_mcmd(preferencesmatrix, lmbda):
        """
        Obtain final alternatives ranking.
        :param preferencesmatrix:
        :param lmbda:
        :return: Alternatives ranking.
        """

        matrix = Intervalar.matrices_IPCM(preferencesmatrix)
        sol = owaop.Owa.c_owg(matrix[0], lmbda)

        return sol




















