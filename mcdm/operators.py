# Implements basic, OWA and OWG operators.
# Basics --> Arithmetic, Geometric and Harmonic
# OWA --> MA-OWA, SMA-OWA, ISMA-OWA
# OWG --> C-OWG, AC-OWG

import numpy as np


class Owa:
    """
    This class implements all necessary methods to calculate OWA (SMA, MA, ISMA) and basic operators.
    """

    def __init__(self):
        pass

    @staticmethod
    def calcutate_arithmetic_mean(valuations):
        """
        Calculates arithmetic mean from valuations list
        :param valuations:
        :return: arithmetic mean
        """

        return np.mean(valuations)

    @staticmethod
    def calculate_geometric_mean(valuations):
        """
        Calculates geometric mean from valuations list
        :param valuations:
        :return: geometric mean
        """

        length = len(valuations)
        gmean = 1
        for x in valuations:
            gmean = x*gmean

        gmean = gmean**(1/length)
        return gmean

    @staticmethod
    def calculate_harmonic_mean(valuations):
        """
        Calculates harmonic mean from valuations list
        :param valuations:
        :return: harmonic mean
        """

        length = len(valuations)
        hmean = 0
        for x in valuations:
            hmean = hmean + 1/x

        hmean = length/hmean
        return hmean

    @staticmethod
    def _takeuniques(valuations):
        """
        Takes uniques values and left values from valuations.
        :param valuations:
        :return: Uniques values and left values
        """
        v = valuations.copy()
        uniques = np.unique(v)
        left = []
        for x in uniques:
            repeated = v[v[:] == x]
            if repeated.shape[0] > 1:
                cropped = repeated[:-1]
                left += cropped.tolist()
        left = np.asarray(left)
        return uniques, left

    @staticmethod
    def _calculatevalues(valuations, delta):
        """
        This method calculates Owa values from uniques list and delta value.
        :param valuations:
        :param delta:
        :return: Owa values
        """
        lessdelta = 0
        lessdeltacount = 0
        firstiterationuniques = Owa._takeuniques(valuations)[0]
        storevalues = (sum([x * delta for x in firstiterationuniques]) / float(len(firstiterationuniques) * delta))
        remainingvaluations = Owa._takeuniques(valuations)[1]

        while len(remainingvaluations) > 0:

            nextuniques = Owa._takeuniques(remainingvaluations)[0]
            for x in firstiterationuniques:
                if x not in nextuniques:
                    lessdeltacount = lessdeltacount + 1
                    lessdelta = lessdelta + x * (1 - delta)

            storevalues = storevalues + (sum([x * delta for x in nextuniques])) + lessdelta
            denominator = (1 + len(nextuniques) * delta) + (len(firstiterationuniques) - len(nextuniques)) * (1 - delta)
            storevalues = storevalues / denominator
            remainingvaluations = Owa._takeuniques(remainingvaluations)[1]
            lessdeltacount = 0
            lessdelta = 0

        return storevalues

    @staticmethod
    def calculate_sma_owa(valuations, delta):
        """
        Calculates SMA-OWA method
        :param valuations:
        :param delta:
        :return: SMA-OWA value
        """
        value = Owa._calculatevalues(valuations, delta)
        return value

    @staticmethod
    def calculate_ma_owa(valuations):
        """
        Calculates MA-OWA method
        :param valuations:
        :return: MA-OWA value
        """
        value = Owa._calculatevalues(valuations, delta=1)
        return value

    @staticmethod
    def calculate_isma_owa(valuations, delta):
        """
        Calculates ISMA-OWA method (Interval SMA-OWA)
        :param valuations:
        :param delta:
        :return: ISMA-OWA value interval
        """

        upper = np.array(valuations)
        lower = np.array(valuations)
        median = np.mean(valuations)

        upper = np.asarray(sorted(upper[upper >= median], reverse=True))
        lower = np.asarray(sorted(lower[lower < median], reverse=True))
        upper = Owa.calculate_sma_owa(upper, delta)
        try:
            lower = Owa.calculate_sma_owa(lower, delta)
        except ZeroDivisionError as e:
            lower = upper

        return [lower, upper]

    @staticmethod
    def c_owg(preferencesmatrix, lmbda):
        """
        Calculates C_OWG operator
        :param preferencesmatrix:
        :param lmbda:
        :return: Ranking of alternatives
        """

        phiList = []
        geometriclist = []
        optionlist = []
        dimension = preferencesmatrix.shape[0]

        for x in range(dimension):
            for y in range(dimension):
                if x<=y:
                    phi = (preferencesmatrix[x][y][0]) * ((preferencesmatrix[x][y][1] / preferencesmatrix[x][y][0])) ** lmbda
                    phiList.append(phi)
                else:
                    phi = (1 / preferencesmatrix[y][x][0]) * ((preferencesmatrix[y][x][0] / preferencesmatrix[y][x][1]) ** lmbda)
                    phiList.append(phi)
            optionlist.append(x+1)
            geometric = Owa.calculate_geometric_mean(phiList)
            geometriclist.append(geometric)
            phiList = []

        sol = sorted(zip(geometriclist, optionlist), reverse=True)

        return sol

    @staticmethod
    def ac_owg(preferencesmatrix, lmbda):
        """
        Calculates AC-OWG Operator
        :param preferencesmatrix:
        :param lmbda:
        :return: Ranking of alternatives
        """

        listaphi = []
        geometriclist = []
        optionlist = []
        dimension = preferencesmatrix.shape[0]

        for x in range(dimension):
            for y in range(dimension):

                    phi = preferencesmatrix[x][y][0] * ((preferencesmatrix[x][y][1]/preferencesmatrix[x][y][0])**lmbda)
                    listaphi.append(phi)

            optionlist.append(x + 1)
            geometric = Owa.calculate_geometric_mean(listaphi)
            geometriclist.append(geometric)
            listaphi = []

            sol = sorted(zip(geometriclist, optionlist), reverse=True)

        return sol



