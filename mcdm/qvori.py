#Implementation of Quorum Valuation Opinion Reputation Index (QV-ORI)

import mcdm.operators as owaop
import mcdm.interval as intvalar

class Ori:
    """
    This class implements all necesary methods to calculate QV_ORI index.

    """

    def __init__(self):
        pass

    @staticmethod
    def qv_ori(valuations, delta):
        """
        Calculate QV-ORI index with ISMA-OWA operator and IRI index.
        :param valuations:
        :param delta:
        :return: Ranking of QV-ORI index.
        """

        ismaowa = owaop.Owa.calculate_isma_owa(valuations, delta)
        iri = intvalar.Intervalar.interval_reability_index(ismaowa)

        qvori = (ismaowa, iri)

        return qvori