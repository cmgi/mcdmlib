#Implement Reptrak-Pulse method calculating weights with EMPV

import numpy as np
import mcdm.empv as empv
from scipy.stats import pearsonr

class reptrakpulse:
    """
    This class implements all necesary methods to calculate Reptrak-Pulse with EMPV.
    """
    def __init__(self):
        pass


    @staticmethod
    def _calculateexperiencessum(dimension, experiences):

        """
        Calculate all experiences.
        :param dimension:
        :param experiences:
        :return: Sum of experiences.
        """

        sum = 0
        totalexperiences = []

        for y in range(len(experiences)):
            for x in range(len(dimension)):
                sum = sum + experiences[y][x]
            totalexperiences.append(sum)
            sum = 0

        return totalexperiences

    @staticmethod
    def _normalizevalues(dimensions, experiences, epsilon):

        """
        Normalize values from epsilon, total list and experiences list.
        :param dimensions:
        :param experiences:
        :param epsilon:
        :return: Normalized experiences, epsilon and total.
        """

        total = reptrakpulse._calculateexperiencessum(dimensions, experiences)

        for x in range(len(experiences)):
            epsilon[x] = (epsilon[x]-1)/6
            total[x] = (total[x] - 4) / 24

        for x in range(len(experiences)):
            for y in range(len(dimensions)):
                experiences[x][y] = (experiences[x][y] - 1)/6

        experiences = np.transpose(experiences)

        return (experiences, epsilon, total)

    @staticmethod
    def _validatecorrelations(experiences, epsilon):

        """
        Calculate Pearson correlation.
        :param experiences:
        :param epsilon:
        :return: Correlation list and epsilon correlation list.
        """

        normexp = reptrakpulse._normalizevalues(experiences, epsilon)[0]
        normepsilon = reptrakpulse._normalizevalues(experiences, epsilon)[1]
        normtotal = reptrakpulse._normalizevalues(experiences, epsilon)[2]
        correlationlist = []
        epsiloncorrelationlist = []

        for x in range(len(normexp)):
            correlationlist.append(pearsonr(normexp[x], normtotal))
            epsiloncorrelationlist.append(pearsonr(normexp[x], normepsilon))

        return (correlationlist, epsiloncorrelationlist)

    @staticmethod
    def _orderlists(dimensions, experiences):

        """
        Order list to calculate weights with EMPV.
        :param dimensions:
        :param experiences:
        :return: List of weights.
        """

        voteslist = []
        sortedvoteslist = []
        empvlist = []

        for x in range(len(experiences)):
            ziplist = zip(experiences[x], dimensions)
            voteslist.append(list(ziplist))

        for x in range(len(voteslist)):
            sortedvotes = sorted(voteslist[x], reverse=True)
            sortedvoteslist.append(sortedvotes)

        for x in range(len(sortedvoteslist)):
            for y in range(len(dimensions)):
                empvlist.append(sortedvoteslist[x][y][1])

        resizelist = np.resize(empvlist,(len(sortedvoteslist), len(dimensions)))
        return resizelist

    @staticmethod
    def _reptrak_empv(dimensions, experiences):

        """
        Calculate reptrak weights with EMPV.
        :param dimensions:
        :param experiences:
        :return: Reptrak Weights.
        """

        voteslist = reptrakpulse._orderlists(dimensions, experiences)
        sol = empv.Empv(dimensions, voteslist)
        return sol.empv()

    @staticmethod
    def calculate_sentiments(dimensions, experiences):
        """
        Calculate Sentiments values with reptrak weights.
        :param dimensions:
        :param experiences:
        :return: Sentiments values.
        """

        sentimentlist = []
        res = 0

        empvlist = reptrakpulse._reptrak_empv(dimensions, experiences)
        for x in range(len(experiences)):
            for y in range(len(dimensions)):
                res = res + (experiences[x][y] * empvlist[y][0])
            sentimentlist.append(res)
            res = 0

        return sentimentlist

    @staticmethod
    def calculate_reptrakpulse(dimensions, experiences, epsilon):

        """
        Calculate Reptrak-Pulse with sentiments values.
        :param dimensions:
        :param experiences:
        :param epsilon:
        :return: Reptrak-Pulse and correlation.
        """

        res = reptrakpulse.calculate_sentiments(dimensions, experiences)
        normepsilon = reptrakpulse._normalizevalues(dimensions, experiences, epsilon)[1]
        correlation = pearsonr(res, normepsilon)
        res = np.mean(res)

        return (res, correlation)



