import mcdm.ahp as ahp
import numpy as np

matrix = np.ones((3, 3))

matrix[0, 1] = 2
matrix[0, 2] = 6
matrix[1, 2] = 3
matrix[1, 0] = 1 / 2
matrix[2, 0] = 1 / 6
matrix[2, 1] = 1 / 3

a1 = [6, 7, 5]
a2 = [2, 6, 2]
a3 = [8, 6, 8]

a = ahp.Ahp(matrix)
b = ahp.AhpConsistency(a)

print("AHP alternative 1: ", a.ahp(a1))
print("AHP alternative 2: ", a.ahp(a2))
print("AHP alternative 3: ", a.ahp(a3), "\n")
print("Normalized Autovector:", a.get_criteria_weights())

matrixprueba = np.ones((4,4))

matrixprueba[0][0] = 1
matrixprueba[0][1] = 4
matrixprueba[0][2] = 5
matrixprueba[0][3] = 1/3
matrixprueba[1][0] = 1/4
matrixprueba[1][1] = 1
matrixprueba[1][2] = 4
matrixprueba[1][3] = 1/3
matrixprueba[2][0] = 1/5
matrixprueba[2][1] = 1/4
matrixprueba[2][2] = 1
matrixprueba[2][3] = 1/7
matrixprueba[3][0] = 3
matrixprueba[3][1] = 3
matrixprueba[3][2] = 7
matrixprueba[3][3] = 1
