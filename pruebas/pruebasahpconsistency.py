import mcdm.ahp as ahp
import numpy as np

matrix = np.ones((3, 3))
matrix[0][1] = 4
matrix[0][2] = 6
matrix[1][2] = 2
matrix[1][0] = 1/4
matrix[2][0] = 1/6
matrix[2][1] = 1/2

matrix2 = np.ones((4, 4))
matrix2[0][1] = 3
matrix2[0][2] = 1/7
matrix2[0][3] = 4
matrix2[1][0] = 1/3
matrix2[1][2] = 7
matrix2[1][3] = 1/9
matrix2[2][0] = 7
matrix2[2][1] = 1/7
matrix2[2][3] = 1/2
matrix2[3][0] = 1/4
matrix2[3][1] = 9
matrix2[3][2] = 2

print(matrix)


print("\n")

a = ahp.Ahp(matrix)
b = ahp.AhpConsistency(a)

print("Saaty Consistency Index -->", b.ci())
print("Saaty Consistency Ratio -->", b.cr())
print("Ci* Index -->", b.ci_star())
print("Ci+ Index -->", b.ci_plus())
print("Cr+ Ratio -->", b.cr_plus())
print("\n")


examplematrix1 = np.ones((4, 4))
examplematrix1[0][1] = 1/7
examplematrix1[0][2] = 1/7
examplematrix1[0][3] = 1/5
examplematrix1[1][0] = 7
examplematrix1[1][2] = 1/2
examplematrix1[1][3] = 1/3
examplematrix1[2][0] = 7
examplematrix1[2][1] = 2
examplematrix1[2][3] = 1/9
examplematrix1[3][0] = 5
examplematrix1[3][1] = 3
examplematrix1[3][2] = 9

res1 = ahp.Ahp(examplematrix1)
res2 = ahp.AhpConsistency(res1)

maxeigenvalue1 = res1._autovalues(examplematrix1)[0]
print("Example 1 solutions: \n")
print(np.round(maxeigenvalue1, 3))
print(np.round(res2.cr(), 3))
print(np.round(res2.ci_star(), 3), "\n")


examplematrix2 = np.ones((4, 4))
examplematrix2[0][1] = 1/5
examplematrix2[0][2] = 1/3
examplematrix2[0][3] = 1/9
examplematrix2[1][0] = 5
examplematrix2[1][2] = 4
examplematrix2[1][3] = 1/8
examplematrix2[2][0] = 3
examplematrix2[2][1] = 1/4
examplematrix2[2][3] = 1/9
examplematrix2[3][0] = 9
examplematrix2[3][1] = 8
examplematrix2[3][2] = 9

res3 = ahp.Ahp(examplematrix2)
res4 = ahp.AhpConsistency(res3)

maxeigenvalue2 = res3._autovalues(examplematrix2)[0]
maxeigenvalue2 = np.round(maxeigenvalue2, 2)
cr2 = np.round(res4.cr(), 2)
cistar2 = np.round(res4.ci_star(), 3)
print("Example 2 solutions: \n")
print(np.round(maxeigenvalue2, 2))
print(np.round(res4.cr(), 2))
print(np.round(res4.ci_star(), 3), "\n")


example3matrix = np.ones((4, 4))
example3matrix[0][1] = 1/3
example3matrix[0][2] = 1/7
example3matrix[0][3] = 1/9
example3matrix[1][0] = 3
example3matrix[1][2] = 1/2
example3matrix[1][3] = 1/5
example3matrix[2][0] = 7
example3matrix[2][1] = 2
example3matrix[2][3] = 1/7
example3matrix[3][0] = 9
example3matrix[3][1] = 5
example3matrix[3][2] = 7

res5 = ahp.Ahp(example3matrix)
res6 = ahp.AhpConsistency(res5)

maxeigenvalue3 = res5._autovalues(example3matrix)[0]
maxeigenvalue3 = np.round(maxeigenvalue3, 3)
cr3 = np.round(res6.cr(), 3)
cistar3 = np.round(res6.ci_star(), 3)
print("Example 3 solutions: \n")
print(np.round(maxeigenvalue3, 3))
print(np.round(res6.cr(), 3))
print(np.round(res6.ci_star(), 3), "\n")



example4matrix = np.ones((4, 4))
example4matrix[0][1] = 1/3
example4matrix[0][2] = 1/7
example4matrix[0][3] = 1/9
example4matrix[1][0] = 3
example4matrix[1][2] = 1/2
example4matrix[1][3] = 1/5
example4matrix[2][0] = 7
example4matrix[2][1] = 2
example4matrix[2][3] = 1/5
example4matrix[3][0] = 9
example4matrix[3][1] = 5
example4matrix[3][2] = 5

res7 = ahp.Ahp(example4matrix)
res8 = ahp.AhpConsistency(res7)

maxeigenvalue4 = res7._autovalues(example4matrix)[0]
maxeigenvalue4 = np.round(maxeigenvalue4, 3)
cr4 = np.round(res8.cr(), 3)
cistar4 = np.round(res8.ci_star(), 3)
print("Example 4 solutions: \n")
print(np.round(maxeigenvalue4, 3))
print(np.round(res8.cr(), 3))
print(np.round(res8.ci_star(), 3), "\n")


