import mcdm.harker as hkr
import numpy as np

position = (1,2)

matriz = np.ones((5,5))
matriz[0][1] = 2
matriz[0][2] = 0
matriz[0][3] = 3
matriz[0][4] = 2
matriz[1][0] = 1/2
matriz[1][2] = 0
matriz[1][3] = 2
matriz[1][4] = 3
matriz[2][0] = 1/3
matriz[2][1] = 1/2
matriz[2][3] = 4
matriz[2][4] = 3
matriz[3][0] = 1/3
matriz[3][1] = 1/2
matriz[3][2] = 1/4
matriz[3][4] = 2
matriz[4][0] = 1/4
matriz[4][1] = 1/3
matriz[4][2] = 1/3
matriz[4][3] = 1/2



matriz2 = np.ones((3,3))
matriz2[0][1] = 2
matriz2[0][2] = 0
matriz2[1][2] = 4
matriz2[1][0] = 1/2
matriz2[2][1] = 1/4

pos2 = (0,2)

sol = hkr.Harkerprediction.prediction(position, matriz)
print("La matriz resultante es:", sol)

sol2 = hkr.Harkerprediction.prediction(pos2, matriz2)
sol3 = hkr.Harkerprediction.prediction((0,2), sol)
print("La matriz resultante es:", sol2)
print("La matriz completa (una vez encontrados los dos valores de 0):", sol3)