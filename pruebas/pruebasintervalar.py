import mcdm.operators as owa
import mcdm.interval as intvalar

import numpy as np

valuations = [0.8, 0.8, 0.6, 0.5, 0.3, 0.3]
delta = 1

intervalmatrix = np.ones((4,4), dtype=tuple)
deflationmatrix = np.ones((4,4), dtype=tuple)

intervalmatrix[0][0] = (0.500, 0.500)
intervalmatrix[0][1] = (0.3976, 0.4024)
intervalmatrix[0][2] = (0.3980, 0.4032)
intervalmatrix[0][3] = (0.5976, 0.6020)
intervalmatrix[1][0] = (0.5976, 0.6024)
intervalmatrix[1][1] = (0.500, 0.500)
intervalmatrix[1][2] = (0.5961, 0.6024)
intervalmatrix[1][3] = (0.5976, 0.6008)
intervalmatrix[2][0] = (0.5968, 0.6020)
intervalmatrix[2][1] = (0.3976, 0.4039)
intervalmatrix[2][2] = (0.500, 0.500)
intervalmatrix[2][3] = (0.5975, 0.6024)
intervalmatrix[3][0] = (0.3980, 0.4024)
intervalmatrix[3][1] = (0.3992, 0.4024)
intervalmatrix[3][2] = (0.3976, 0.4025)
intervalmatrix[3][3] = (0.500, 0.500)

deflationmatrix[0][0] = (0.500, 0.500)
deflationmatrix[0][1] = (0.397, 0.402)
deflationmatrix[0][2] = (0.398, 0.403)
deflationmatrix[0][3] = (0.597, 0.602)
deflationmatrix[1][0] = (0.597, 0.602)
deflationmatrix[1][1] = (0.500, 0.500)
deflationmatrix[1][2] = (0.596, 0.602)
deflationmatrix[1][3] = (0.597, 0.600)
deflationmatrix[2][0] = (0.596, 0.691)
deflationmatrix[2][1] = (0.397, 0.403)
deflationmatrix[2][2] = (0.500, 0.500)
deflationmatrix[2][3] = (0.597, 0.602)
deflationmatrix[3][0] = (0.398, 0.402)
deflationmatrix[3][1] = (0.399, 0.402)
deflationmatrix[3][2] = (0.397, 0.402)
deflationmatrix[3][3] = (0.500, 0.500)

yagermatrix = np.ones((5,5), dtype=tuple)

yagermatrix[0][0] = (1,1)
yagermatrix[0][1] = (1/4, 1/3)
yagermatrix[0][2] = (1/2, 2)
yagermatrix[0][3] = (2, 4)
yagermatrix[0][4] = (7, 8)
yagermatrix[1][0] = (3,4)
yagermatrix[1][1] = (1,1)
yagermatrix[1][2] = (2,3)
yagermatrix[1][3] = (1/6, 1/4)
yagermatrix[1][4] = (3,5)
yagermatrix[2][0] = (1/2, 2)
yagermatrix[2][1] = (1/3, 1/2)
yagermatrix[2][2] = (1, 1)
yagermatrix[2][3] = (4,5)
yagermatrix[2][4] = (1,2)
yagermatrix[3][0] = (1/4, 1/2)
yagermatrix[3][1] = (4, 6)
yagermatrix[3][2] = (1/5, 1/4)
yagermatrix[3][3] = (1, 1)
yagermatrix[3][4] = (1/4, 1/3)
yagermatrix[4][0] = (1/8, 1/7)
yagermatrix[4][1] = (1/5, 1/3)
yagermatrix[4][2] = (1/2, 1)
yagermatrix[4][3] = (3, 4)
yagermatrix[4][4] = (1, 1)


sol = intvalar.Intervalar.ici_plus(intervalmatrix)
sol2 = intvalar.Intervalar.matrices_IPCM(intervalmatrix)

print("Intervalar Consistency Index -->", sol)
print("Matrix Deflation (Already Deflate) -->", sol2)

sol3 = owa.Owa.ac_owg(intervalmatrix, 1/2)
print("AC-OWG:", sol3)

sol4 = owa.Owa.c_owg(yagermatrix, 2/3)
print("C-OWG:", sol4)








