import mcdm.operators as owa
import numpy as np

listone = np.random.randint(1, 5, size=10000)
listtwo = np.random.randint(90, 100, size=20)

listthree = np.concatenate((listone, listtwo), axis=0)

print("Generating list of random numbers...")
print("10000 numbers between 1 y 5...")
print("20 numbers between 90 y 100...")
print(listthree)

a = owa.Owa()

print("Mean of list --> ", listthree.mean())
print("SMA-OWA with delta=0.75 -->", a.calculate_sma_owa(listthree, 0.75))
print("SMA-OWA with delta=1 (Also MA-OWA) -->", a.calculate_sma_owa(listthree, 1))
print("ISMA-OWA with delta=1", a.calculate_isma_owa(listthree, 1), "\n")

owalist = [0.7, 0.7, 0.7, 0.7, 0.5, 0.4, 0.1, 0.1, 0.1]
owalist = np.asarray(owalist)

print("Next list:", owalist)
print("MA-OWA of previous list -->", a.calculate_ma_owa(owalist))
print("ISMA-OWA of previous list with delta = 0.25 -->", a.calculate_isma_owa(owalist, 0.25), "\n")


owalist2 = [0.4, 0.4, 0.4, 0.4, 0.6, 0.6, 0.3]
owalist2 = np.asarray(owalist2)

print("Next list:", owalist2)
print("MA-OWA of previous list -->", a.calculate_ma_owa(owalist2))
print("ISMA-OWA os previous list with delta = 1 -->", a.calculate_isma_owa(owalist2, 1), "\n")


owalist3 = [1, 5, 5, 5, 5, 5, 5, 5, 5]
owalist3 = np.asarray(owalist3)

print("Next list:", owalist3)
print("SMA-OWA with delta=0,8 -->", a.calculate_sma_owa(owalist3, 0.8))
print("ISMA-OWA with delta = 0,8 -->", a.calculate_isma_owa(owalist3, 0.8), "\n")


owalist4 = [0.8, 0.8, 0.6, 0.5, 0.3, 0.3]
owalist4 = np.asarray(owalist4)

print("Next list:", owalist4)
print("ISMA-OWA with delta=1 -->", a.calculate_isma_owa(owalist4, 1), "\n")



