import mcdm.qvori as ori


hotel1valuations = [-0.0449, 0.9370]
hotel2valuations = [-0.9388, 0.9690]
hotel3valuations = [-0.8497, -0.0231]
hotel4valuations = [-0.5013, 0.0005]

qvori1 = ori.Ori.qv_ori(hotel1valuations, 1)
qvori2 = ori.Ori.qv_ori(hotel2valuations, 1)
qvori3 = ori.Ori.qv_ori(hotel3valuations, 1)
qvori4 = ori.Ori.qv_ori(hotel4valuations, 1)


print(qvori1)
print(qvori2)
print(qvori3)
print(qvori4)

