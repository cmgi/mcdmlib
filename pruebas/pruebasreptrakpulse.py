import mcdm.reptrakpulse as rp

alternatives = ['A', 'B', 'C', 'D']
experiences = [[2,3,2,1], [6,5,6,6], [1,2,1,1], [7,7,6,7], [5,5,5,5]]
epsilon = [2,7,2,6,5]

sol = rp.reptrakpulse.calculate_reptrakpulse(alternatives, experiences, epsilon)
print("La solución, junto con la correlación es: ", sol)