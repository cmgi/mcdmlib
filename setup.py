from setuptools import setup

setup(name="mcdm",
      version="0.1.12",
      description="Multicriterial Decision Methods",
      url="http://cmgi.uma.es",
      author="CM&GI",
      author_email="info@cmgi.uma.es",
      license="MIT",
      packages=["mcdm"],
      install_requires=["numpy"],
      zip_safe=False)
