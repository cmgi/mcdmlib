import unittest
from mcdm.ahp import *


class AhpTests(unittest.TestCase):
    """ This class tests AHP method."""
    def test_matrix_of_ones(self):
        """ Testing with all ones matrix."""
        a = Ahp(np.ones((3, 3)))
        res = a.ahp([1, 1, 1])

        self.assertEqual(res, 1)
        self.assertIsInstance(res, float)

    def test_madeup_matrix(self):
        """Testing with random matrix."""
        matrix = [[1, 2, 3],
                  [1/2, 1, 6],
                  [1/3, 1/6, 1]]
        a = Ahp(matrix)
        res = a.ahp([2, 3, 4])
        print(res)
        self.assertGreater(res, 2)

class AhpConsistencyTests(unittest.TestCase):
    """ This class probes AHPConsistency."""
    def test_ahpconsistency(self):
        """Testing consistency indexes and ratios"""
        matrix = [[1, 2, 6],
                  [1/2, 1, 3],
                  [1/6, 1/3, 1]]

        matrix = np.asmatrix(matrix)

        a = Ahp(matrix)
        b = AhpConsistency(a)

        res = b.cr()
        res2 = b.ci()

        if abs(res2) < 1e-10:
            return 0

        res3 = b.ci_plus()

        self.assertEqual(res, 0)
        self.assertEqual(res2, 0)
        self.assertEqual(res3, 1)


    def test_ci_star_index(self):
        "Testing that ci* index result is between 0 and 1"
        matrix = np.ones((4,4))
        matrix[0][1] = 5
        matrix[0][2] = 7
        matrix[0][3] = 9
        matrix[1][0] = 1/5
        matrix[2][0] = 1/7
        matrix[3][0] = 1/9
        matrix[1][2] = 2
        matrix[1][3] = 4
        matrix[2][3] = 10
        matrix[2][1] = 1/2
        matrix[3][1] = 1/4
        matrix[3][2] = 1/10

        a = Ahp(matrix)
        b = AhpConsistency(a)

        res = b.ci_plus() #Saaty scale

        self.assertTrue(res[0] >= 0 and res[0] <= 1)

    def test_transitivity(self):
        """Testing that the number of transivities in paper has relation with our recursive decomposer function."""

        matrix = np.ones((8,8))

        dimension = matrix.shape[0]

        a = Ahp(matrix)
        b = AhpConsistency(a)

        res = b.recursive_decomposer(matrix)  # Transitividades (Para 4x4 = 4)
        formula = (AhpConsistency._factorial(dimension)) / (AhpConsistency._factorial(3))

        self.assertEqual(formula, len(res))

    def test_consistency(self):
        """Testing that indices values are 0 and 1 respectly when a matrix is consistent."""

        matrix = np.ones((3,3))
        matrix[0][1] = 2
        matrix[0][2] = 8
        matrix[1][0] = 1/2
        matrix[2][0] = 1/8
        matrix[1][2] = 4
        matrix[2][1] = 1/4

        matrix2 = np.ones((3,3))
        matrix2[0][1] = 3
        matrix2[0][2] = 5
        matrix2[1][0] = 1/3
        matrix2[2][0] = 1/5
        matrix2[1][2] = 8
        matrix2[2][1] = 1/8

        a = Ahp(matrix)
        b = AhpConsistency(a)

        c = Ahp(matrix2)
        d = AhpConsistency(c)

        res = b.ci_plus()
        res2 = b.ci_star()

        res3 = d.ci_plus()
        res4 = d.ci_star()

        self.assertEqual(res[0], 1)
        self.assertEqual(res2, 0)

        self.assertNotEqual(res3[0], 1)
        self.assertNotEqual(res4, 0)

    def test_ci_star_paper(self):
        """Testing that our index returns exactly the same that examples of 'A new measure of consistency for
           positive reciprocal matrices. (J.I. Peláez, M.T. Lamata)(2003).
           In page 1844 there are four examples that returns:
                Example 1: Maximum Eigenvalue --> 4.828, CR --> 0.306, CI* --> 4.446
                Example 2: Maximum Eigenvalue --> 4.38, CR --> 0.14, CI* --> 1,664
                Example 3: Maximum Eigenvalue --> 4.275, CR --> 0.102, CI* --> 1,268
                Example 4: Maximum Eigenvalue --> 4.167, CR --> 0.061, CI* --> 0.741
            We prove that our values minus paper values have an insignificant difference equal or less 0.01"""

        #EXAMPLE 1
        exampleMatrix1 = np.ones((4, 4))
        exampleMatrix1[0][1] = 1 / 7
        exampleMatrix1[0][2] = 1 / 7
        exampleMatrix1[0][3] = 1 / 5
        exampleMatrix1[1][0] = 7
        exampleMatrix1[1][2] = 1 / 2
        exampleMatrix1[1][3] = 1 / 3
        exampleMatrix1[2][0] = 7
        exampleMatrix1[2][1] = 2
        exampleMatrix1[2][3] = 1 / 9
        exampleMatrix1[3][0] = 5
        exampleMatrix1[3][1] = 3
        exampleMatrix1[3][2] = 9

        res1 = Ahp(exampleMatrix1)
        res2 = AhpConsistency(res1)

        maxeigenvalue1 = res1._autovalues(exampleMatrix1)[0]
        cr1 = res2.cr()
        cistar1 = res2.ci_star()
        print("Example 1 solutions: \n")
        print(maxeigenvalue1)
        print(res2.cr())
        print(res2.ci_star(), "\n")

        self.assertLessEqual(np.subtract(maxeigenvalue1, 4.828), 0.01)
        self.assertLessEqual(np.subtract(cr1, 0.306), 0.01)
        self.assertLessEqual(np.subtract(cistar1, 4.446), 0.01)

        #EXAMPLE 2
        exampleMatrix2 = np.ones((4, 4))
        exampleMatrix2[0][1] = 1 / 5
        exampleMatrix2[0][2] = 1 / 3
        exampleMatrix2[0][3] = 1 / 9
        exampleMatrix2[1][0] = 5
        exampleMatrix2[1][2] = 4
        exampleMatrix2[1][3] = 1 / 8
        exampleMatrix2[2][0] = 3
        exampleMatrix2[2][1] = 1/4
        exampleMatrix2[2][3] = 1 / 9
        exampleMatrix2[3][0] = 9
        exampleMatrix2[3][1] = 8
        exampleMatrix2[3][2] = 9

        res3 = Ahp(exampleMatrix2)
        res4 = AhpConsistency(res3)

        maxeigenvalue2 = res3._autovalues(exampleMatrix2)[0]
        cr2 = res4.cr()
        cistar2 = res4.ci_star()
        print("Example 2 solutions: \n")
        print(maxeigenvalue2)
        print(res4.cr())
        print(res4.ci_star(), "\n")

        self.assertLessEqual(np.subtract(maxeigenvalue2, 4.38), 0.01)
        self.assertLessEqual(np.subtract(cr2, 0.14), 0.01)
        self.assertLessEqual(np.subtract(cistar2, 1.664), 0.01)

        # EXAMPLE 3
        exampleMatrix3 = np.ones((4, 4))
        exampleMatrix3[0][1] = 1 / 3
        exampleMatrix3[0][2] = 1 / 7
        exampleMatrix3[0][3] = 1 / 9
        exampleMatrix3[1][0] = 3
        exampleMatrix3[1][2] = 1 / 2
        exampleMatrix3[1][3] = 1 / 5
        exampleMatrix3[2][0] = 7
        exampleMatrix3[2][1] = 2
        exampleMatrix3[2][3] = 1 / 7
        exampleMatrix3[3][0] = 9
        exampleMatrix3[3][1] = 5
        exampleMatrix3[3][2] = 7

        res5 = Ahp(exampleMatrix3)
        res6 = AhpConsistency(res5)

        maxeigenvalue3 = res5._autovalues(exampleMatrix3)[0]
        cr3 = res6.cr()
        cistar3 = res6.ci_star()
        print("Example 3 solutions: \n")
        print(maxeigenvalue3)
        print(res6.cr())
        print(res6.ci_star(), "\n")

        self.assertLessEqual(np.subtract(maxeigenvalue3, 4.275), 0.01)
        self.assertLessEqual(np.subtract(cr3, 0.102), 0.01)
        self.assertLessEqual(np.subtract(cistar3, 1.269), 0.01)

        # EXAMPLE 4
        exampleMatrix4 = np.ones((4, 4))
        exampleMatrix4[0][1] = 1 / 3
        exampleMatrix4[0][2] = 1 / 7
        exampleMatrix4[0][3] = 1 / 9
        exampleMatrix4[1][0] = 3
        exampleMatrix4[1][2] = 1 / 2
        exampleMatrix4[1][3] = 1 / 5
        exampleMatrix4[2][0] = 7
        exampleMatrix4[2][1] = 2
        exampleMatrix4[2][3] = 1 / 5
        exampleMatrix4[3][0] = 9
        exampleMatrix4[3][1] = 5
        exampleMatrix4[3][2] = 5

        res7 = Ahp(exampleMatrix4)
        res8 = AhpConsistency(res7)

        maxeigenvalue4 = res7._autovalues(exampleMatrix4)[0]
        cr4 = res8.cr()
        cistar4 = res8.ci_star()
        print("Example 4 solutions: \n")
        print(maxeigenvalue4)
        print(res8.cr())
        print(res8.ci_star(), "\n")

        self.assertLessEqual(np.subtract(maxeigenvalue4, 4.168), 0.01)
        self.assertLessEqual(np.subtract(cr4, 0.061), 0.01)
        self.assertLessEqual(np.subtract(cistar4, 0.734), 0.01)


if __name__ == '__main__':
    unittest.main()
