import unittest
import mcdm.cmgi as cmgiindex
import numpy as np


class cmgiTests(unittest.TestCase):
    """This class tests CMGI Index"""

    def test_cmgi_fouralternatives(self):
        """Testing CMGI index from paper 'CMGI Sinergy Index: A Qualitative Online Reputation Metric'.
           In paper solution for four alternatives is (0.3745,1), (0.2161, 3), (0.2845, 2), (0.1248, 4)"""

        listapolaridadesred = [[1, 0, 0, 0], [0, 0, 1, -1], [0, 1, 0, 0], [1, -1, 0, -1]]
        res = [(0.3745, 1), (0.2162, 3), (0.2845, 2), (0.1248, 4)]
        ressol = []
        sol = cmgiindex.cmgi.calculatecmgi(listapolaridadesred)

        for x in range (0,4):
            decimalsvalue = np.round(((sol[x])[0]), 4)
            sol.append(decimalsvalue)
            ressol.append(res[x])

        self.assertEqual(ressol, res)





if __name__ == '__main__':
    unittest.main()
