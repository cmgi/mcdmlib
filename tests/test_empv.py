import unittest
import mcdm.empv as empv
import numpy as np



class EmpvTests(unittest.TestCase):
    """This class tests EMPV method"""

    def test_paper(self):
        """In this example we going to test if our EMPV implementation returns exactly the same that 'Pairwise voting
           to rank tourist destinations based on preference valuation' paper, the solution will be A = 0.63919,
           B = 0.14221, C = 0.21860 """

        alternativeList = ['A', 'B', 'C']

        votesList = (
        ['A', 'B', 'C'], ['A', 'B', 'C'], ['A', 'B', 'C'], ['A', 'B', 'C'], ['A', 'B', 'C'], ['A', 'C', 'B'],
        ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'],
        ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'],
        ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['A', 'C', 'B'], ['B', 'A', 'C'],
        ['B', 'A', 'C'], ['B', 'C', 'A'], ['B', 'C', 'A'], ['B', 'C', 'A'], ['B', 'C', 'A'], ['C', 'A', 'B'],
        ['C', 'A', 'B'], ['C', 'A', 'B'])

        a = empv.Empv(alternativeList, votesList)
        c = []

        for x in range(len(a.empv())):
            c.append((np.round((a.empv()[x])[0], 5), (a.empv()[x])[1]))

        b = [(0.63919, 'A'), (0.21860, 'C'), (0.14221, 'B')]
        self.assertEqual(b, c)


if __name__ == '__main__':
    unittest.main()
