import unittest
import numpy as np
import mcdm.interval as intvalar


class intervalarTests(unittest.TestCase):
    """ This class tests intervalar methods"""

    def test_ici_plus(self):
        """ Testing ICI+ from paper 'Decision Making in social media with consistent data' (J.I. Peláez, Eustaquio A.
            Martínez, Luis G. Vargas). In paper solution is ICI+(M) = 0.989"""

        intervalmatrix = np.ones((4, 4), dtype=tuple)

        intervalmatrix[0][0] = (0.500, 0.500)
        intervalmatrix[0][1] = (0.300, 0.500)
        intervalmatrix[0][2] = (0.300, 0.500)
        intervalmatrix[0][3] = (0.499, 0.699)
        intervalmatrix[1][0] = (0.499, 0.700)
        intervalmatrix[1][1] = (0.500, 0.500)
        intervalmatrix[1][2] = (0.498, 0.700)
        intervalmatrix[1][3] = (0.499, 0.698)
        intervalmatrix[2][0] = (0.499, 0.699)
        intervalmatrix[2][1] = (0.299, 0.501)
        intervalmatrix[2][2] = (0.500, 0.500)
        intervalmatrix[2][3] = (0.499, 0.700)
        intervalmatrix[3][0] = (0.300, 0.500)
        intervalmatrix[3][1] = (0.301, 0.500)
        intervalmatrix[3][2] = (0.300, 0.500)
        intervalmatrix[3][3] = (0.500, 0.500)

        res = 0.989
        iciplus = intvalar.Intervalar.ici_plus(intervalmatrix)

        iciplus = np.round(iciplus, decimals=3)

        self.assertEqual(res, iciplus)



if __name__ == '__main__':
    unittest.main()
