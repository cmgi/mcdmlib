import unittest
from mcdm.operators import *
import numpy as np


class OperatorsTests(unittest.TestCase):
    """This class tests OWA methods"""

    def test_ma_owa(self):
        """Testing that MA-OWA returns exactly the same that SMA-OWA with delta = 1"""
        list = np.asarray([7, 7, 7, 7, 4, 4, 4, 1])
        list2 = np.asarray([8, 6, 6, 2, 3, 1, 1, 4])
        a = Owa()
        res = a.calculate_sma_owa(list, 1)
        res2 = a.calculate_ma_owa(list)

        res3 = a.calculate_sma_owa(list2, 1)
        res4 = a.calculate_ma_owa(list2)

        self.assertEqual(res, res2)
        self.assertEqual(res3, res4)

    def test_owa_paper(self):
        """ Testing that paper solution 'Majority OWA operator for opinion rating
            in social media' (J.I. Peláez, R.Bernal, M.Karanik) it's the same that out method solution

            First example the solution is: 0,551 (Pag. 1050)
            Second example the solution is: 0,419 (Pag. 1050)"""

        owaList = [0.7, 0.7, 0.7, 0.7, 0.5, 0.4, 0.1, 0.1, 0.1]
        owaList = np.array(owaList)
        res = 0.551
        a = Owa()
        b = np.round(a.calculate_ma_owa(owaList),3)

        self.assertEqual(res, b)

        owaList2 = [0.4, 0.4, 0.4, 0.4, 0.6, 0.6, 0.3]
        owaList2 = np.array(owaList2)
        res2 = 0.419
        b = np.round(a.calculate_ma_owa(owaList2),3)

        self.assertEqual(res2, b)

    def test_isma_owa_paper(self):
        """ Testing ISMA-OWA from paper 'Decision Making in social media with consistent data' (J.I. Peláez, Eustaquio A.
            Martínez, Luis G. Vargas). In paper the interval solution is [0.35, 0.75]"""

        delta = 1
        valuationsList = [0.8, 0.8, 0.6, 0.5, 0.3, 0.3]
        res = [0.35, 0.75]

        a = Owa()
        b = a.calculate_isma_owa(valuationsList, delta)

        self.assertEqual(b, res)

    def test_c_owg_yager(self):
        """Testing C-OWG from paper 'The continuous ordered weighted geometric operator and its application to decision
           making' (Ronald R. Yager, Zeshui Xu). In paper alternatives ranking is [(1.5611, 1), (1.5145, 2), (1.1742, 3)
           (0.6233, 4), (0.5779, 5)] with BUM = 2/3. We use self.greaterequal because solution differs by 0.001"""

        yagermatrix = np.ones((5, 5), dtype=tuple)

        yagermatrix[0][0] = (1, 1)
        yagermatrix[0][1] = (1 / 4, 1 / 3)
        yagermatrix[0][2] = (1 / 2, 2)
        yagermatrix[0][3] = (2, 4)
        yagermatrix[0][4] = (7, 8)
        yagermatrix[1][0] = (3, 4)
        yagermatrix[1][1] = (1, 1)
        yagermatrix[1][2] = (2, 3)
        yagermatrix[1][3] = (1 / 6, 1 / 4)
        yagermatrix[1][4] = (3, 5)
        yagermatrix[2][0] = (1 / 2, 2)
        yagermatrix[2][1] = (1 / 3, 1 / 2)
        yagermatrix[2][2] = (1, 1)
        yagermatrix[2][3] = (4, 5)
        yagermatrix[2][4] = (1, 2)
        yagermatrix[3][0] = (1 / 4, 1 / 2)
        yagermatrix[3][1] = (4, 6)
        yagermatrix[3][2] = (1 / 5, 1 / 4)
        yagermatrix[3][3] = (1, 1)
        yagermatrix[3][4] = (1 / 4, 1 / 3)
        yagermatrix[4][0] = (1 / 8, 1 / 7)
        yagermatrix[4][1] = (1 / 5, 1 / 3)
        yagermatrix[4][2] = (1 / 2, 1)
        yagermatrix[4][3] = (3, 4)
        yagermatrix[4][4] = (1, 1)

        sol = []
        ressol = []
        a = Owa.c_owg(yagermatrix, 2/3)

        res = [(1.561, 1), (1.514, 2), (1.174, 3), (0.623, 4), (0.577, 5)]

        for x in range (0,5):
           decimalsvalue = np.round(((a[x])[0]),3)
           sol.append(decimalsvalue)
           ressol.append((res[x])[0])

        self.assertGreaterEqual(sol, ressol)





if __name__ == '__main__':
    unittest.main()
