import unittest
import mcdm.qvori as ori
import numpy as np


class qvOriTests(unittest.TestCase):
    """This class tests valuations indexes methods"""

    def test_qv_ori(self):
        """"Testing that this qv-ori returns the same result as the paper 'Products and services valuation through
            unsolicited information from social media' (J.I. Peláez, Eustaquio A. Martínez, Luis G. Vargas)"""

        hotel1valuations = [-0.0449, 0.9370]
        hotel2valuations = [-0.9388, 0.9690]
        hotel3valuations = [-0.8497, -0.0231]
        hotel4valuations = [-0.5013, 0.0005]

        #By default delta = 1

        qvori1 = ori.Ori.qv_ori(hotel1valuations, 1)
        qvori2 = ori.Ori.qv_ori(hotel2valuations, 1)
        qvori3 = ori.Ori.qv_ori(hotel3valuations, 1)
        qvori4 = ori.Ori.qv_ori(hotel4valuations, 1)

        qvori1 = (qvori1[0], np.round(qvori1[1], decimals=4))
        qvori2 = (qvori2[0], np.round(qvori2[1], decimals=4))
        qvori3 = (qvori3[0], np.round(qvori3[1], decimals=3))
        qvori4 = (qvori4[0], np.round(qvori4[1], decimals=4))


        paperqvori1 = ([-0.0449, 0.9370], 0.9085)
        paperqvori2 = ([-0.9388, 0.9690], 0.0158)
        paperqvori3 = ([-0.8497, -0.0231], -0.9470)
        paperqvori4 = ([-0.5013, 0.0005], -0.9980)

        self.assertEqual(qvori1, paperqvori1)
        self.assertEqual(qvori2, paperqvori2)
        self.assertEqual(qvori3, paperqvori3)
        self.assertEqual(qvori4, paperqvori4)




if __name__ == '__main__':
    unittest.main()
